This project uses a **major**.**minor**.**micro** versioning scheme, where:

* Bumping the major version resets the minor version to zero.
* Bumping the minor version resets the micro version to zero.
* The major version is bumped on breaking changes to the `stacking-iterator` crate as defined by Rust RFC 1122.
* The minor version is bumped on minor changes to the `stacking-iterator` crate, as defined by Rust RFC 1122.
* The micro version is bumped in all other cases.

# v1.0.0

This is the first release.
