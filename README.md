# stacking-iterator

Iterator utilities for manipulating stacks.

For more information, see the [crate docs](https://docs.rs/stacking-iterator).

## License

Available via the [Anti-Capitalist Software License][ACSL] for individuals, non-profit
organisations, and worker-owned businesses.

[ACSL]: ./LICENSE.md
